function NPrice() {
    let s = document.getElementsByName("Type");
    let select = s[0];
    let price = 0;
    let prices = TPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
      price = prices.prodTypes[priceIndex];
    }
    
  
    let radioDiv = document.getElementById("RadioButton");
    radioDiv.style.display = (select.value == "2" ? "block" : "none");
    let RadioButton = document.getElementsByName("RadioOptions");
    RadioButton.forEach(function(radio) {
      if (radio.checked) {
        let optionPrice = prices.RadioOptions[radio.value];
        if (optionPrice !== undefined) {
          price += optionPrice;
        }
      }
    });
  
  
    let checkDiv = document.getElementById("checkboxes");
    checkDiv.style.display = (select.value == "3" ? "block" : "none");
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
      if (checkbox.checked) {
        let propPrice = prices.prodProperties[checkbox.name];
        if (propPrice !== undefined) {
          price += propPrice;
        }
      }
    });
    
  
    let Price = document.getElementById("Price");
    Price.innerHTML = price + " Рублей";
  }
  function TPrices() {
    return {
      prodTypes: [150000, 0, 0],
      RadioOptions: {
        option1:20000,
        option2:7000,
        option3: 1500,
        option4: 5000,
      },
      prodProperties: {
        CheckName1: 20000,
        CheckName2: 70000,
        CheckName3: 400000,
        CheckName4: 50000,
      }
    };
  }
  
  
  window.addEventListener('DOMContentLoaded', function (event) {
    let radioDiv = document.getElementById("RadioButton");
    radioDiv.style.display = "none";
    let s = document.getElementsByName("Type");
    let select = s[0];
    select.addEventListener("change", function(event) {
      let target = event.target;
      console.log(target.value);
      NPrice();
    });
     
    
    let RadioButton = document.getElementsByName("RadioOptions");
    RadioButton.forEach(function(radio) {
      radio.addEventListener("change", function(event) {
        let r = event.target;
        console.log(r.value);
        NPrice();
      });
    });
  
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
      checkbox.addEventListener("change", function(event) {
        let c = event.target;
        console.log(c.name);
        console.log(c.value);
        NPrice();
      });
    });
    NPrice();
  });
  